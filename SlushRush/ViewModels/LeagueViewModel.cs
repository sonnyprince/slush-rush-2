﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SlushRush.ViewModels
{
    public class LeagueViewModel
    {
        public string UserName { get; set; }
        public int Score { get; set; }
    }
}