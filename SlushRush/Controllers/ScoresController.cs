﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using SlushRush.Models;
using SlushRush.Services;
using SlushRush.ViewModels;
using System.Web.Http.Cors;

namespace SlushRush.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ScoresController : ApiController
    {
        private SlushRushContext db = new SlushRushContext();

        // GET: api/Scores
        public List<LeagueViewModel> GetScores(int limit)
        {
            using (var sdb = new SlushRushContext())
            {
                var scores = sdb.Scores.ToList();
                var league = scores.Select(score => new LeagueViewModel() { UserName = score.User.Username, Score = score.UserScore }).OrderByDescending(x => x.Score).Take(limit).ToList();
                return league;
            }
        }

        // POST: api/Scores
        [ResponseType(typeof(Score))]
        public HttpStatusCode PostScore(ScoreViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return HttpStatusCode.BadRequest;
            }

            var user = UserService.GetUser(model.UserName, model.UserEmail);
            db.Scores.Add(new Score() { UserScore = model.Score, DateTime = DateTime.Now, UserId = user.UserId });
            db.SaveChanges();

            return HttpStatusCode.OK;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}