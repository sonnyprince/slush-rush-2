window.formHelper = (function ($) {

    var user = {
      form: $('#user-form-submission'),
      formSubmission: $('.user-form-submission'),
      formReSubmission: $('.submitUser')
    };

    function updateSubmissionStatus(data) {
      $('#user-form-submission').hide();
      if(data == 200) {
        $('.successful-submission').show();
      }else {
        $('.unsuccessful-submission').show();
      }
    }

    function submitUser() {
        var formUsername = $('#user-form-submission').find('#username').val();
        var formEmail = $('#user-form-submission').find('#useremail').val();

        var obj = {"UserName": formUsername, "UserEmail": formEmail, "Score": scores.finalScore};
        apiService.postScore(obj);
    };

    //set up validation for each form
    function init() {
        $('form').each(function() {
            $(this).validate();
        });

        $(user.form).submit(function(e){
          e.preventDefault();
          if($(this).valid()){
            submitUser();
          }
        });

        $(user.formReSubmission).click(function(e){
          e.preventDefault();
          submitUser();
        });
    };

    init();

    return {
        init: init,
        updateSubmissionStatus: updateSubmissionStatus
    };

})(jQuery);
