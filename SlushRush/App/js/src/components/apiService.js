"use strict";

var apiService = (function ($) {

  var api = {
    requestUri: '/api/Scores',
    mockGet: [{"UserName": "Willy p","Score": 40},{"UserName": "Rob","Score": 28},{"UserName": "Sonny","Score": 26},{"UserName": "Willy p","Score": 10}],
    mockPost: {"UserName": 'Test user', "UserEmail": 'sonon@gm.com', "Score": '540', }
  }

  function getScores(limit){
    // return api.mockGet;
    $.get(api.requestUri + '?limit=' + limit, function(data) {
       leaderBoard.buildLeaderBoard(data);
    });
	};

  function postScore(userObj) {
    var obj = userObj; //change to userObj when not in test.
    $.post(api.requestUri, obj, function(data) {
      formHelper.updateSubmissionStatus(data);
    });
  }

	return {
    postScore: postScore,
    getScores: getScores
	};

})(jQuery);
