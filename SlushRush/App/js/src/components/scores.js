"use strict";

var scores = (function ($) {

	var score = {
		elm: '',
		value: '',
		count: 1,
		parent: $('.score-count'),
		incrementHolder: $('.score-increment'),
		incrementHolderValue: $('.score-incremnet-number'),
		showClass: 'score-increment-show',
		current: $('.score-count-number').data('score'),
		number: $('.score-count-number'),
		final: 0,
		finalElm: $('.final-score-number')
	};

	function finalScore() {
		return score.final;
	};

	function updateFinalUI() {
		score.finalElm.text(finalScore());
	};

	function resetScores() {
		score.final = score.current;
		score.current = 000;
		score.number.data('score', score.current).attr('data-score', score.current).html(score.current);
	};

	function createVisualScore() {
		//If there are more than 6 score elements start getting rid
		if (score.count > 6) {
			$('.score-increment-' + (score.count - 6)).remove();
		}
		//Create each visual score element (we add a unique class so its easier to remove)
		score.elm = '<div class="score-increment score-increment-' + score.count + '">+' + score.value + '</div>';
		//Add the visual score element to the main
		$('main').append(score.elm);
		//The next frame after creation we add the show class so it animates in
		requestNextAnimationFrame(function () {
			$('.score-increment-' + (score.count - 1)).addClass(score.showClass);
		});
		//increment the score count (this is used for the unique classes)
		score.count++;
	};

	function scoreIncrement(value) {
		score.value = value;
		//Show the user they've got some points
		if (!gamePlay.onTabletOrBelow()) {
			createVisualScore();
		}
		//update the current score with the new score
		score.current = parseInt(score.current) + parseInt(score.value);
		//update the score data attr (both attr and data are used for DOM and ui) and display
		score.number.data('score', score.current).attr('data-score', score.current).html(score.current);
	};

	function init(points) {
		scoreIncrement(points);
	};

	return {
		init: init,
		resetScores: resetScores,
		finalScore: finalScore,
		updateFinalUI: updateFinalUI
	};

})(jQuery);
