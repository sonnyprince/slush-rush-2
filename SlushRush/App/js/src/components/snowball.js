"use strict";

var snowballs = (function ($) {

	var snowball = {
		x: '',
		y: '',
		elm: '',
		count: '',
	};

	function removeSnowballs() {
		$('.snowball').remove();
	}

	function createSnowball() {
		//If there are more than 6 snowball elements start getting rid
		if (snowball.count > 6) {
			$('.snowball-' + (snowball.count - 6)).remove();
		}
		//Create each snowball element (we add a unique class so its easier to remove)
		snowball.elm = '<div class="snowball snowball-' + snowball.count + '"></div>';
		//Add the snowball element to the main
		$('main').append(snowball.elm);
	}

	function snowballThrow() {
		//increment the score count (this is used for the unique classes)
		snowball.count++;
		//create a snowball
		createSnowball();
		//Play the throw sound
		soundEffects.init('throw');
		//The next frame after creation we add the show class and set the end position so it animates in
		requestNextAnimationFrame(function () {
			$('.snowball-' + snowball.count).css({
				'top' : snowball.y,
				'left' : snowball.x,
			})
			.addClass('thrown');
		});
		setTimeout(function() {
			//Fire the collision function to see is we hit (500 timeout matches the throw animation)
			collision.init(snowball.x, snowball.y);
		}, 500);
	}

	function init() {
		$('body').on('click touchend', function(e) {
			if (!$(this).hasClass('game-inactive')) {
				//set the snowball end X and Y to the co-ords of a click
				snowball.x = e.pageX;
				snowball.y = e.pageY;
				//throw the snowball
				snowballThrow();
			}
		});
	};

	return {
		init: init,
		removeSnowballs: removeSnowballs
	};

})(jQuery); 