"use strict";

var collision = (function ($) {

	var collide = {
		data: '',
		oldData: '',
		sbX: '',
		sbY: '',
		santaElm: $('.santa'),
		santaElmPos: '',
		santaElmRightX: '',
		santaElmBottomY: '',
		hitScore: '',
	};

	function compareX() {
		//console.log('snowball X:' + collide.sbX);
		//console.log('between :' + collide.santaElmPos.left + 'and : ' + collide.santaElmRightX);
		//If our snowballs X is between the top left X and top right X return true
		if (collide.sbX >= collide.santaElmPos.left && collide.sbX <= collide.santaElmRightX) {
			return true;
		} else {
			return false;
		}
	}
	function compareY() {
		//console.log('snowball Y:' + collide.sbY);
		//console.log('between :' + collide.santaElmPos.top + 'and : ' + collide.santaElmBottomY);
		//If our snowballs Y is between the top left Y and bottom left Y return true
		if (collide.sbY >= collide.santaElmPos.top && collide.sbY <= collide.santaElmBottomY) {
			return true;
		} else {
			return false;
		}
	}

	function collisionDetection() {
		//did we sit within the Xs and Ys of a santa?????
		if (compareY() && compareX()) {
			//if hell yeah we did we fire the scores and pass through the hit score amount (set front setCuttentPoint())
			scores.init(collide.hitScore);
			//Play the hit sound
			soundEffects.init('hit');
		}
	}

	function setSantaPositions(santa) {
		//Get the santas offsets
		collide.santaElmPos = $(santa).offset();
		//get the top right co-ords by addings top left and width
		collide.santaElmRightX = (collide.santaElmPos.left + $(santa).outerWidth());
		//get the bottom left co-ords by addings top left and height
		collide.santaElmBottomY = (collide.santaElmPos.top + $(santa).outerHeight());
		//check if our snowball hit
		collisionDetection();
	}

	function pointSort(type) {
		//Look at type, compare with levels and return point number
		if (type === 'easy') {
			return 10;
		} else if (type === 'medium') {
			return 25;
		} else if (type === 'hard') {
			return 50;
		}
	}

	function setCurrentPoint(pointElm) {
		//get the data p (which is the point level)
		var pointLevel = $(pointElm).data('p');
		//Set the point number to the hitscore var (pointSort turns data attr string to point)
		collide.hitScore = pointSort(pointLevel);
	}


	function init(sbx, sby) {
		//set the X and Y of the snowball
		collide.sbX = sbx - 15;
		collide.sbY = sby;
		//Loop through each of the santas
		collide.santaElm.each(function() {
			//set the current point level and coordinates of the santas
			setCurrentPoint($(this));
			setSantaPositions($(this));
		})
		//setSantaPositions();
	};

	return {
		init: init
	};

})(jQuery); 