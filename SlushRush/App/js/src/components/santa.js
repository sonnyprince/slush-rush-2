"use strict";

var runningSanta = (function ($) {

	var santas = {
		number: '',
		santa: $('.santa'),
		run: 'run',
		total: 3,
		addedClasses: [],
	};

	Array.prototype.contains = function(elem) {
		for (var i in this) {
			if (this[i] == elem) return true;
		}
		return false;
	}

	function stopRunning() {
		$('.santa').removeClass(santas.run);
	}

	function startRunning() {
		santas.number = Math.floor(Math.random() * 3) + 1;
		
		if (santas.addedClasses.contains(santas.number)) {
			startRunning();
		} else {
			santas.addedClasses.push(santas.number);
			$('.santa-' + santas.number).addClass(santas.run);
		}

	}

	function fireTheRun() {
		for (i = 0; i < santas.total; i++) { 
			var rand = Math.round(Math.random() * (3000 - 500)) + 500;
			setTimeout(function() {
				startRunning();
			}, rand);
		}		

	}

	function init() {
		fireTheRun();
		santas.addedClasses = [];
	};

	return {
		init: init,
		stopRunning: stopRunning
	};

})(jQuery); 