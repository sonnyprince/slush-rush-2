"use strict";

var soundEffects = (function ($) {

	var sfx = {
		sound: '',
		muted: false,
		loop: $('.audio-loop'),
	};

	function playAudio(choice) {
		sfx.sound = new Audio('App/SFX/' + choice + '.wav');
		sfx.sound.play();
	}

	function init(choice) {
		if (sfx.muted === false && !gamePlay.onTabletOrBelow()) {
			playAudio(choice);
		}
	};

	function loop(playStop) {
		if (playStop === 'play') {
			sfx.loop.get(0).play();
		} else if (playStop === 'stop') {
			sfx.loop.get(0).pause();
		}
	};

	function load() {
		if (gamePlay.onTabletOrBelow()) {
			sfx.loop.get(0).pause();
		} else {
			sfx.loop.get(0).play();
		}
	}

	return {
		init: init,
		sfx: sfx,
		loop: loop,
		load: load,
	};

})(jQuery); 