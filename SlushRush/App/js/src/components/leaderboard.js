"use strict";

var leaderBoard = (function ($) {

	var leaderboard = {
		container: $('.leaderboard-data')
	};

	function getEntryStructure(rank, score, name) {
		return '<li class="leaderboard-row data-row">'
					+ '<div class="leaderboard-cell data-cell">'
						+ '<p class="leaderboard-cell-title">' + (rank + 1) +'</p>'
					+ '</div>'
					+ '<div class="leaderboard-cell data-cell">'
						+ '<p class="leaderboard-cell-title">' + name +'</p>'
					+ '</div>'
					+ '<div class="leaderboard-cell data-cell">'
						+ '<p class="leaderboard-cell-title">' + score + '</p>'
					+ '</div>'
				+ '</li>';
	};

	function buildLeaderBoard(data) {
		$(leaderboard.container).empty();
		$(data).each(function(iteration, item){
			$(leaderboard.container).append(getEntryStructure(iteration, item.Score, item.UserName));
		});
	}

	function getLeaderBoard() {
		apiService.getScores(5);
	}

	return {
		getLeaderBoard: getLeaderBoard,
		buildLeaderBoard: buildLeaderBoard
	};

})(jQuery);
