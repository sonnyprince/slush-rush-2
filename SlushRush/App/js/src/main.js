"use strict";

var gamePlay = (function ($) {

	var game = {
		body: $('body'),
		hide: 'hide',
		show: 'show',
		window: $('.game-window'),
		inner: $('.game-window-inner'),
		leaderboard: $('.leaderboard-inner'),
		scoreBlock: $('.score-count'),
		timerBlock: $('.game-timer'),
		gameInactive: 'game-inactive',
		gameOver: 'game-over',
		gameStart: 'game-start',
		loader: $('.game-loading'),
		windowWidth: $(window).width(),
	};

	function gameWindow(whatToDo) {
		if (whatToDo === game.hide) {
			game.scoreBlock.fadeIn()
			game.timerBlock.fadeIn();
			game.window.fadeOut();
		} else if (whatToDo === game.show) {
			game.window.fadeIn();
			game.scoreBlock.fadeOut()
			game.timerBlock.fadeOut();
		}
	};

	function onTabletOrBelow() {
		if (game.windowWidth <=1024) {
			return true;
		} else {
			return false 
		}
	};

	//prevent/enable text selection in IE
	function IETextSelection(action) {
		document.onselectstart = function() {
			return action;
		}
	};

	function startGame() {
		snowballs.init();
		runningSanta.init();
		gameTimer.init();
		setTimeout(function() {
			game.body.removeClass(game.gameStart).removeClass(game.gameInactive).removeClass(game.gameOver);
		}, 100);
		IETextSelection(false);
	};

	function endGame() {
		gameWindow(game.show);
		runningSanta.stopRunning();
		scores.resetScores();
		snowballs.removeSnowballs();
		game.body.addClass(game.gameInactive).addClass(game.gameOver);
		game.inner.hide();
		game.leaderboard.show();
		scores.updateFinalUI();
		IETextSelection(true);
	};

	function init() {

		leaderBoard.getLeaderBoard();
	    FastClick.attach(document.body);

	    Pace.on('done', function() {
	    	if(game.body.hasClass(game.gameStart)) {
	    		setTimeout(function() {
		    		game.loader.fadeOut();
	  				gameWindow(game.show);
	  				leaderBoard.getLeaderBoard();
				}, 1500);
	    	}
		});

	    //start game btn click
		$('.start-game-play').on('click', function(e) {
			gameWindow(game.hide);
			soundEffects.init('click');
			startGame();
			leaderBoard.getLeaderBoard();
			e.preventDefault();

			$('#user-form-submission').fadeIn();
    		$('.successful-submission').fadeOut();
      		$('.unsuccessful-submission').fadeOut();
		});

		//view leaderboard button
		$('.view-leader-board').on('click', function(e) {
			leaderBoard.getLeaderBoard();
			e.preventDefault();
		});

		//toggle sound control UI
		$('.sound-control').on('click', function(e) {
			e.preventDefault();
			if (!onTabletOrBelow()) {
				var self = $(this),
					isMuted = true ? self.hasClass('icon-volume-off') : false,
					removeClass = '',
					addClass = '';

				if(isMuted === true) {
					removeClass = "icon-volume-off";
					addClass = "icon-volume-on";
					isMuted = false;
					soundEffects.loop('play');

				} else {
					removeClass = "icon-volume-on";
					addClass = "icon-volume-off";
					isMuted = true;
					soundEffects.loop('stop');

				}

				self.removeClass(removeClass).addClass(addClass);
				soundEffects.sfx.muted = isMuted
			}
		});

		//handle show/hide
		$('button[data-hidden-trigger]').on('click', function(){
			var target = $(this).data('hidden-trigger');
			$('div[data-hidden-target]').hide();
			$('div[data-hidden-target="' + target +'"]').show();
		});

		//disable right hand click
		document.addEventListener("contextmenu", function(e){
    		e.preventDefault();
		}, false);

	};

	return {
		init: init,
		endGame: endGame,
		onTabletOrBelow: onTabletOrBelow,
	};

})(jQuery);
