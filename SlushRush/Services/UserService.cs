﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SlushRush.Models;

namespace SlushRush.Services
{
    public static class UserService
    {
        public static User GetUser(string username, string email)
        {
            using (var db = new SlushRushContext())
            {
                User user;

                if (!string.IsNullOrEmpty(email))
                {
                    user = db.Users.FirstOrDefault(a => a.Email == email);
                    if (user != null) return user;
                }

                user = db.Users.Add(new User() {Email = email, Username = username});
                db.SaveChanges();
                return user;
            }
        }
    }
}