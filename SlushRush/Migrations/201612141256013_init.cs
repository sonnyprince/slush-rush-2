namespace SlushRush.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Scores", "User_UserId", "dbo.Users");
            DropIndex("dbo.Scores", new[] { "User_UserId" });
            RenameColumn(table: "dbo.Scores", name: "User_UserId", newName: "UserId");
            AlterColumn("dbo.Scores", "UserId", c => c.Int(nullable: false));
            CreateIndex("dbo.Scores", "UserId");
            AddForeignKey("dbo.Scores", "UserId", "dbo.Users", "UserId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Scores", "UserId", "dbo.Users");
            DropIndex("dbo.Scores", new[] { "UserId" });
            AlterColumn("dbo.Scores", "UserId", c => c.Int());
            RenameColumn(table: "dbo.Scores", name: "UserId", newName: "User_UserId");
            CreateIndex("dbo.Scores", "User_UserId");
            AddForeignKey("dbo.Scores", "User_UserId", "dbo.Users", "UserId");
        }
    }
}
