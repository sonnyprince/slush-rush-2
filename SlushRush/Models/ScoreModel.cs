﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace SlushRush.Models
{
    public class Score
    {
        public int ScoreId { get; set; }
        public int UserScore { get; set; }
        public DateTime DateTime { get; set; }
        public int UserId { get; set; }

        [ForeignKey("UserId")]
        public virtual User User { get; set; }
    }
}