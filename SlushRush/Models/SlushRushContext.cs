﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SlushRush.Models
{
    public class SlushRushContext : DbContext
    {
        public SlushRushContext() : base("name=SlushRush"){}

        public DbSet<User> Users { get; set; }
        public DbSet<Score> Scores { get; set; }
    }
}