﻿using System.Collections;
using System.Collections.Generic;

namespace SlushRush.Models
{
    public class User
    {
        public User()
        {
            Scores = new List<Score>();
        }

        public int UserId { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public virtual ICollection<Score> Scores { get; set; }   
    }
}